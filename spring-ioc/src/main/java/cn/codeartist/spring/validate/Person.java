package cn.codeartist.spring.validate;

import lombok.Data;

/**
 * @author AiJiangnan
 * @date 2023-12-06
 */
@Data
public class Person {

    private String name;
    private Integer age;
}
