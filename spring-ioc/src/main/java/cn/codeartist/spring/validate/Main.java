package cn.codeartist.spring.validate;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * @author AiJiangnan
 * @date 2023-12-06
 */
public class Main {

    public static void main(String[] args) {
        Person person = new Person();
        person.setName("ajn");
        person.setAge(12232);
        Errors errors = new BeanPropertyBindingResult(person, "person");
        ValidationUtils.invokeValidator(new PersonValidator(), person, errors);
        System.out.println(errors.getAllErrors());
    }
}
