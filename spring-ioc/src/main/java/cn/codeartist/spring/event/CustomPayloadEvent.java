package cn.codeartist.spring.event;

import org.springframework.context.PayloadApplicationEvent;

public class CustomPayloadEvent extends PayloadApplicationEvent<String> {

    private final String name;

    public CustomPayloadEvent(Object source, String name) {
        super(source, name);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
