package cn.codeartist.spring.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.stereotype.Component;

@Component
public class CustomService implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher publisher;

    public void publish() {
        CustomEvent event = new CustomEvent(this, "CodeArtist");
        PayloadApplicationEvent<String> payload = new PayloadApplicationEvent<>(this, "Payload");
        publisher.publishEvent(event);
        publisher.publishEvent(payload);
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }
}
