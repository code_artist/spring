package cn.codeartist.spring.bean.xml;

public class BeanProvider {

    public BeanProvider() {
        System.out.println("ProviderBean.ProviderBean");
    }

    public void sayHello(String name, Integer year) {
        System.out.println("name = [" + name + "], year = [" + year + "]");
    }
}
