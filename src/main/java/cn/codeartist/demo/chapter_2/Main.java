package cn.codeartist.demo.chapter_2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("chapter_2/bean.xml");
        ExampleBean exampleBean = applicationContext.getBean("exampleBean", ExampleBean.class);
    }
}
